package com.example.tripmap;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

//import afu.org.checkerframework.checker.nullness.qual.NonNull;

public class SplashScreen extends AppCompatActivity {


    private boolean mLocationPermissionGranted = false;
    public static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    public static final String COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    public static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    boolean gps_enabled = false;
    boolean network_enabled = false;
    LocationManager lm;
    private String[] permission = {FINE_LOCATION, COARSE_LOCATION};
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        mLocationPermissionGranted = true;

        switch (requestCode){
            case LOCATION_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0){
                    for (int i=0; i<grantResults.length; i++){
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED){
//                            mLocationPermissionGranted = false;
                            return;
                        }
                    }

//                    finish();

                }
//                if(permissions.length > 0){
//                    for (int i=0; i<permissions.length; i++){
//                        if (!permissions[i].equals(PackageManager.PERMISSION_GRANTED)){
////                            mLocationPermissionGranted = false;
//                            return;
//                        }
//                    }
//                }

                if (isLocationEnabled(getApplicationContext())) {
                    Log.d(TAG, "onRequestPermissionsResult: enabled gps");
                    Intent intent = new Intent(this, MapsActivity.class);
                    startActivity(intent);
                }else {
                    Log.d(TAG, "onRequestPermissionsResult: no gps");
                    Intent intent1 = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent1);
                }

        }
//        if (isLocationEnabled(getApplicationContext())) {
//            Log.d(TAG, "onRequestPermissionsResult: enabled gps");
//            Intent intent = new Intent(this, MapsActivity.class);
//            startActivity(intent);
//        }else {
//            Log.d(TAG, "onRequestPermissionsResult: no gps");
//            Intent intent1 = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//            startActivity(intent1);
//        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }

    public static Boolean isLocationEnabled(Context context)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
// This is new method provided in API 28
            LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            return lm.isLocationEnabled();
        } else {
// This is Deprecated in API 28
            int mode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE,
                    Settings.Secure.LOCATION_MODE_OFF);
            return  (mode != Settings.Secure.LOCATION_MODE_OFF);

        }
    }

    private static final String TAG = "SplashScreen";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
//        LocationManager lm = (LocationManager)getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(), FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){//Fine location Permission grant check
            if (ContextCompat.checkSelfPermission(this.getApplicationContext(), COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED){//Coarse location permission check

                mLocationPermissionGranted = true;
//                initMap();//initialising the map if the permission is granted
                Handler handler=new Handler();
                if(isLocationEnabled(getApplicationContext()))
                {
                    Log.d(TAG, "onCreate: Enabled GPS");
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(SplashScreen.this, MapsActivity.class);
                            startActivity(intent);
//                        finish();
                        }
                    }, 500);
                }else {
                    Log.d(TAG, "onCreate: Not Enabled");
                    Intent intent1 = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent1);
                }
//                finish();
            }
        }
        if (!mLocationPermissionGranted){
            ActivityCompat.requestPermissions(this, permission, LOCATION_PERMISSION_REQUEST_CODE);

    }
    }
}

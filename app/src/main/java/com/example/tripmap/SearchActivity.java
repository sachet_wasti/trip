package com.example.tripmap;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsResponse;
import com.google.android.libraries.places.api.net.PlacesClient;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity implements View.OnClickListener, RecyclerItemClickListener.OnRecyclerClickListener {

    View v;
    AutoCompleteTextView search_location;
    private PlacesClient placesClient;
    private static final String TAG = "SearchActivity";
    private List<AutocompletePrediction> list;
    ImageView search;
    ArrayList<String> arr;
    RecyclerView placesRecyclerView;
    PlacesAdapter placesAdapter;

    String location;
    private static SearchActivity onSearchData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        myToolbar.setNavigationIcon(R.drawable.ic_arrow_back);

        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(getApplicationContext(), MapsActivity.class));
            }
        });

        arr = new ArrayList<>();

        search_location = findViewById(R.id.search_location);
        search = findViewById(R.id.search);

        placesRecyclerView = findViewById(R.id.placesrecyclerview);

        placesRecyclerView.setNestedScrollingEnabled(false);

        Places.initialize(this, "AIzaSyDq-zxxwG6z1lzWXqHh_1gTtHqvaPA35IY");
        placesClient = Places.createClient(this);
        final AutocompleteSessionToken token = AutocompleteSessionToken.newInstance();

        placesRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, placesRecyclerView, this));

        search_location.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                final FindAutocompletePredictionsRequest predictionsRequest = FindAutocompletePredictionsRequest.builder()
                        .setTypeFilter(TypeFilter.ADDRESS)
                        .setSessionToken(token)
                        .setQuery(s.toString())
                        .build();

                placesClient.findAutocompletePredictions(predictionsRequest).addOnCompleteListener(new OnCompleteListener<FindAutocompletePredictionsResponse>() {
                    @Override
                    public void onComplete(@NonNull Task<FindAutocompletePredictionsResponse> task) {
                        if (task.isSuccessful()) {//find out the prediction from Google

                            FindAutocompletePredictionsResponse predictionsResponse = task.getResult();

                            if (predictionsResponse != null) {
                                Log.d(TAG, "onComplete: got the list");
                                list = predictionsResponse.getAutocompletePredictions();
                            }

                            arr = new ArrayList<>();

                            for (AutocompletePrediction a : list) {
                                arr.add(a.getSecondaryText(null).toString());
                            }

                            Log.d(TAG, "onComplete: got the list "+arr);

//
                            placesRecyclerView.setLayoutManager(new LinearLayoutManager(SearchActivity.this, LinearLayoutManager.VERTICAL, false));
                            placesAdapter = new PlacesAdapter(SearchActivity.this, arr);
                            placesRecyclerView.setAdapter(placesAdapter);
//
                            placesAdapter.notifyDataSetChanged();

                        }
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        search.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (!search_location.getText().toString().equals("")){

            searchPlace();

        }
    }

    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(SearchActivity.this, MapsActivity.class);
        startActivity(intent);
    }


    public void searchPlace(){
        finish();
        Intent intent = new Intent(SearchActivity.this, MapsActivity.class);
        intent.putExtra("LocationData", search_location.getText().toString());
        startActivity(intent);
    }

    @Override
    public void onItemClick(View view, int position) {
        finish();
        Log.d(TAG, "onItemClick: starts");
        Log.d(TAG, "onItemLongClick: starts");
        Intent intent = new Intent(SearchActivity.this, MapsActivity.class);
        intent.putExtra("LocationData", arr.get(position));
        //for this to work the Photo object should be serialisable
        startActivity(intent);
    }

    @Override
    public void onItemLongClick(View view, int position) {
        finish();
        Log.d(TAG, "onItemLongClick: starts");
        Intent intent = new Intent(SearchActivity.this, MapsActivity.class);
        intent.putExtra("LocationData", arr.get(position));
        //for this to work the Photo object should be serialisable
        startActivity(intent);
        /*Using the putExtra method we are telling the details of the photo that we are trying to display*/
        /*Works similar to a bundle*/
    }
}


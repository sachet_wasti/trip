package com.example.tripmap;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.tripmap.Model.ScooterModel;


public class FirstBottomSheetFragment extends BottomSheetDialogFragment {

    private FirstButtonClickListener buttonClickListener;
    private Button confirmBooking;
    public FirstBottomSheetFragment() {
    }


    interface FirstButtonClickListener{
        void onConfirmedButtonClicked(ScooterModel position, int pos);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.firstbottomsheet, container, false);
        confirmBooking = v.findViewById(R.id.confirmBookingId);
        final ScooterModel scooterModel = (ScooterModel) getArguments().getSerializable("Id");
        final int position = (Integer)getArguments().getSerializable("position");


        confirmBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClickListener.onConfirmedButtonClicked(scooterModel, position);
            }
        });
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        buttonClickListener = (FirstBottomSheetFragment.FirstButtonClickListener) context;
    }
}
package com.example.tripmap;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.tripmap.Model.ScooterModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class MapsActivity extends AppCompatActivity implements FirstBottomSheetFragment.FirstButtonClickListener,BottomSheetFragment.ButtonClickListener, NavigationView.OnNavigationItemSelectedListener {
    TabLayout tabLayout;
    ViewPager viewPager;
    Handler handler;
    static final String TAG = "MapsActivity";

    public final static String ACTION_GATT_CONNECTED =
            "com.example.tripmap.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED =
            "com.example.tripmap.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED =
            "com.example.tripmap.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE =
            "com.example.tripmap.ACTION_DATA_AVAILABLE";
    public final static String EXTRA_DATA =
            "com.example.tripmap.EXTRA_DATA";

    private RequestQueue requestQueue;

    private BluetoothGatt mBluetoothGatt;
    private String mBluetoothDeviceAddress;

    private int connectionState = STATE_DISCONNECTED;

    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;

    private String authorizationKey = null;
    private BluetoothGattCharacteristic chara;

    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        sendBroadcast(intent);
    }

    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        /**
         * This is called on a connection state change (either connection or disconnection)
         *
         * @param gatt     The GATT database object
         * @param status   Status of the event
         * @param newState New state (connected or disconnected)
         */
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                String intentAction;
                if (newState == BluetoothProfile.STATE_CONNECTED) {
                    intentAction = ACTION_GATT_CONNECTED;
                    connectionState = STATE_CONNECTED;
                    broadcastUpdate(intentAction);
                    Log.i(TAG, "Connected to GATT server.");
                    Log.i(TAG, "Attempting to start service discovery:" +
                            mBluetoothGatt.discoverServices());

                } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                    intentAction = ACTION_GATT_DISCONNECTED;
                    connectionState = STATE_DISCONNECTED;
                    Log.i(TAG, "Disconnected from GATT server.");
                    broadcastUpdate(intentAction);
                }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            BluetoothGattCharacteristic chara = null;
            if (status == BluetoothGatt.GATT_SUCCESS) {
                BluetoothGattService service = mBluetoothGatt.getService(UUID.fromString("0000ffe0-0000-1000-8000-00805f9b34fb"));
                chara = service.getCharacteristic(UUID.fromString("0000ffe1-0000-1000-8000-00805f9b34fb"));
                MapsActivity.this.chara = chara;

                writeCharacteristic(chara, authorizationKey);

//                dikky = buttomSheetView.findViewById(R.id.dikky);
//                dikky.setEnabled(true);

                broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED); // broadcast Gatt connection state : discovered
            } else {
                Log.e(TAG, "onServicesDiscovered received: " + status);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            Log.i("LOG_BLEServ"," onCharacteristicRead status = "+String.valueOf(status));
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_DATA_AVAILABLE);
            }
        }
    };
    private int position;

    private void setstartTrip(int position){

        final String url = "http://tript.pe.hu/api/vehicle/starttrip/"+(position+1);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "onResponse: "+response+" "+url);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("KeylessFragment", "onErrorResponse: running");
                error.printStackTrace();
            }
        });
        requestQueue.add(request);
    }

    public void writeCharacteristic(BluetoothGattCharacteristic characteristic, String data){
        try{
            characteristic.setValue(URLEncoder.encode(data, "utf-8"));
            Log.d(TAG, "writeCharacteristic: Data "+data+" "+URLEncoder.encode(data, "utf-8"));
            mBluetoothGatt.writeCharacteristic(characteristic);
            stopScanning();

            setstartTrip(position);

            Intent intent = new Intent(this, ResumeEndTrip.class);
            intent.putExtra("Position", position + 1);
            startActivity(intent);

        }catch (UnsupportedEncodingException e){
            e.printStackTrace();
        }
    }


    public boolean connect(final String address) {
        if (mBluetoothAdapter == null || address == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

        // Previously connected device.  Try to reconnect.
        if (mBluetoothDeviceAddress != null && address.equals(mBluetoothDeviceAddress)
                && mBluetoothGatt != null) {
            Log.i(TAG, "Trying to use an existing mBluetoothGatt for connection.");
            for (BluetoothGattService v: mBluetoothGatt.getServices()){
                Log.d(TAG, "connect: "+v.getCharacteristic(UUID.fromString("00001530-0000-3512-2118-0009af100700")));
            }
            return mBluetoothGatt.connect();
        }

        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) {
            Log.w(TAG, "Device not found.  Unable to connect.");
            return false;
        }
        // We want to directly connect to the device, so we are setting the autoConnect
        // parameter to false.
        mBluetoothGatt = device.connectGatt(this, false, mGattCallback);
        Log.i(TAG, "Trying to create a new connection.");
        mBluetoothDeviceAddress = address;
        return true;
    }


        @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.colorAccent));
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(MapsActivity.this);

        viewPager = findViewById(R.id.viewpager);
        tabLayout = findViewById(R.id.tab);
        handler = new Handler();

        requestQueue = Volley.newRequestQueue(this);


        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        Intent i = getIntent();
        String offmessage = null;
        if (i != null){
            offmessage = i.getStringExtra("BluetoothOff");
        }

//        if(offmessage != null && mBluetoothAdapter.isEnabled())mBluetoothAdapter.disable();

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        final PageAdapter pageAdapter = new PageAdapter(getSupportFragmentManager());
        pageAdapter.AddFragment(new KeylessFragment(), "Keyless");
        pageAdapter.AddFragment(new LongTermFragment(), "LongTerm");
        pageAdapter.AddFragment(new ShiftFragment(), "Shift");
        pageAdapter.AddFragment(new TrippassFragment(), "Trip Pass");
        pageAdapter.AddFragment(new BuyFragment(), "Buy");


            viewPager.setAdapter(pageAdapter);
            tabLayout.setupWithViewPager(viewPager);
        createTabIcons();
        for(int j=0; j < tabLayout.getTabCount(); j++) {
            View tab = ((ViewGroup) tabLayout.getChildAt(0)).getChildAt(j);
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) tab.getLayoutParams();
            p.setMargins(45, 0, 45, 0);
            tab.requestLayout();
        }


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void createTabIcons() {
        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setText("Keyless");
        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.icon_keyless, 0, 0);
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setText("LongTerm");
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.icon_longterm, 0, 0);
        tabLayout.getTabAt(1).setCustomView(tabTwo);

        TextView tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabThree.setText("Shift");
        tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.icon_shift, 0, 0);
        tabLayout.getTabAt(2).setCustomView(tabThree);

        TextView tabfour = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabfour.setText("Trip Pass");
        tabfour.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.icon_trippass, 0, 0);
        tabLayout.getTabAt(3).setCustomView(tabfour);

        TextView tabfive = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabfive.setText("Buy");
        tabfive.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.icon_buy, 0, 0);
        tabLayout.getTabAt(4).setCustomView(tabfive);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.offers) {
            // Handle the camera action
        } else if (id == R.id.freeRides) {

        } else if (id == R.id.faq) {

        } else if (id == R.id.faq) {

        } else if (id == R.id.help) {

        } else if (id == R.id.contactUs) {

        }
        else if (id == R.id.rate) {

        }
        else if (id == R.id.about) {

        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onButtonClicked(ScooterModel position) {
        //GET URL FROM THE SCOOTER ID
        String url = getScooterURL(position.getId());
        Log.d(TAG, "onButtonClicked: " + url);
        //DOWNLOAD THE MAC ADDRESS FROM THE VOLLEY;
        getMacAddress(url);


    }

    @Override
    public void onRideCanceled() {
        final String url = "http://tript.pe.hu/api/vehicle/endtrip/"+(this.position+1);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "onResponse: "+response+" "+url);

                finish();

                Intent intent = new Intent(MapsActivity.this, MapsActivity.class);
                startActivity(intent);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("KeylessFragment", "onErrorResponse: running");
                error.printStackTrace();
            }
        });
        requestQueue.add(request);


    }

    BluetoothAdapter mBluetoothAdapter;
    private static final int REQUEST_ENABLE_BT = 2;
    BluetoothLeScanner scanner;
    MyScanCallback myScanCallback;


    //This code will check to see if there is a bluetooth device and
    //turn it on if is it turned off.
    public void startbt(String btmac, String auth) {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
            Log.d(TAG, "startbt: This device does not support bluetooth");
            return;
        }
        //make sure bluetooth is enabled.
        if (!mBluetoothAdapter.isEnabled()) {
            Log.d(TAG, "startbt: There is bluetooth, but turned off");
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        } else {
            Log.d(TAG, "startbt: The bluetooth is ready to use." + auth + " "+ authorizationKey);

            if (auth != null)
                startScanning(btmac);
        }
    }

    public void stopScanning() {
        Log.d(TAG, "stopScanning: RUNNNING....");
        if (scanner != null) {
            scanner.stopScan(myScanCallback);
        }
    }

    public void startScanning(String mac) {
        Log.d(TAG, "startScanning: RUNNNING....");
        scanner = mBluetoothAdapter.getBluetoothLeScanner();
        ScanSettings scanSettings = new ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY).build();
        List<ScanFilter> scanFilters = Arrays.asList(
                new ScanFilter.Builder()
                        .setDeviceAddress(mac)
                        //.setServiceUuid(ParcelUuid.fromString("some uuid"))
                        .build());
        myScanCallback = new MyScanCallback();
        scanner.startScan(scanFilters, scanSettings, myScanCallback);
    }

    @Override
    public void onConfirmedButtonClicked(ScooterModel position, int pos) {

        setNewTrip(pos);
        this.position = pos;

        BottomSheetFragment bottomSheetFragment = new BottomSheetFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("Id", position);
        //Log.d(TAG, "openBottomSheet: " + scooterList.get(position));
        bottomSheetFragment.setArguments(bundle);
        bottomSheetFragment.show(this.getSupportFragmentManager(), bottomSheetFragment.getTag());

    }

    public class MyScanCallback extends ScanCallback {
        @Override
        public void onScanResult(int callbackType, final ScanResult result) {
            //Do something with results
            BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(result.getDevice().getAddress());
            Toast.makeText(MapsActivity.this, "THE DEVICE: " + device.getName() + ", " + device.getAddress(), Toast.LENGTH_SHORT).show();
//            mBluetoothGatt = device.connectGatt(MapsActivity.this, false, mGattCallback);
            connect(device.getAddress());

       }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            //Do something with batch of results
        }

        @Override
        public void onScanFailed(int errorCode) {
            //Handle error
        }
    }
    private void setNewTrip(int position){

        final String url = "http://tript.pe.hu/api/vehicle/newtrip/"+(position+1)+"/1";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "onResponse: "+response+" "+url);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("KeylessFragment", "onErrorResponse: running");
                error.printStackTrace();
            }
        });
        requestQueue.add(request);
    }
    private void getMacAddress(String url){
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject vehicle = response.getJSONObject("vehicle");
                    String btmac = vehicle.getString("btmac");
                    authorizationKey = vehicle.getString("auth");
                    Log.d("MapsActivity", "onResponse: " + btmac);
                    startbt(btmac, authorizationKey);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("MapsActivity", "onErrorResponse: running");
                error.printStackTrace();
            }
        });
        requestQueue.add(request);
    }


    private String getScooterURL(String scooterId) {
        String url = "http://tript.pe.hu/api/vehicle/status/" + scooterId;
        return url;
    }
}
